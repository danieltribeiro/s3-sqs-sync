
package valente.sqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseElements {

    @SerializedName("x-amz-request-id")
    @Expose
    private String xAmzRequestId;
    @SerializedName("x-amz-id-2")
    @Expose
    private String xAmzId2;

    public String getXAmzRequestId() {
        return xAmzRequestId;
    }

    public void setXAmzRequestId(String xAmzRequestId) {
        this.xAmzRequestId = xAmzRequestId;
    }

    public String getXAmzId2() {
        return xAmzId2;
    }

    public void setXAmzId2(String xAmzId2) {
        this.xAmzId2 = xAmzId2;
    }

}
