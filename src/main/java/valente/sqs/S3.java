
package valente.sqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class S3 {

    @SerializedName("s3SchemaVersion")
    @Expose
    private String s3SchemaVersion;
    @SerializedName("configurationId")
    @Expose
    private String configurationId;
    @SerializedName("bucket")
    @Expose
    private Bucket bucket;
    @SerializedName("object")
    @Expose
    private Object object;

    public String getS3SchemaVersion() {
        return s3SchemaVersion;
    }

    public void setS3SchemaVersion(String s3SchemaVersion) {
        this.s3SchemaVersion = s3SchemaVersion;
    }

    public String getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(String configurationId) {
        this.configurationId = configurationId;
    }

    public Bucket getBucket() {
        return bucket;
    }

    public void setBucket(Bucket bucket) {
        this.bucket = bucket;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

}
