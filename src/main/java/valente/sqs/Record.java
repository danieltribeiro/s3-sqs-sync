
package valente.sqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

    @SerializedName("eventVersion")
    @Expose
    private String eventVersion;
    @SerializedName("eventSource")
    @Expose
    private String eventSource;
    @SerializedName("awsRegion")
    @Expose
    private String awsRegion;
    @SerializedName("eventTime")
    @Expose
    private String eventTime;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("userIdentity")
    @Expose
    private UserIdentity userIdentity;
    @SerializedName("requestParameters")
    @Expose
    private RequestParameters requestParameters;
    @SerializedName("responseElements")
    @Expose
    private ResponseElements responseElements;
    @SerializedName("s3")
    @Expose
    private S3 s3;

    public String getEventVersion() {
        return eventVersion;
    }

    public void setEventVersion(String eventVersion) {
        this.eventVersion = eventVersion;
    }

    public String getEventSource() {
        return eventSource;
    }

    public void setEventSource(String eventSource) {
        this.eventSource = eventSource;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public UserIdentity getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(UserIdentity userIdentity) {
        this.userIdentity = userIdentity;
    }

    public RequestParameters getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(RequestParameters requestParameters) {
        this.requestParameters = requestParameters;
    }

    public ResponseElements getResponseElements() {
        return responseElements;
    }

    public void setResponseElements(ResponseElements responseElements) {
        this.responseElements = responseElements;
    }

    public S3 getS3() {
        return s3;
    }

    public void setS3(S3 s3) {
        this.s3 = s3;
    }

}
