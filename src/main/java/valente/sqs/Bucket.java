
package valente.sqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bucket {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ownerIdentity")
    @Expose
    private OwnerIdentity ownerIdentity;
    @SerializedName("arn")
    @Expose
    private String arn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OwnerIdentity getOwnerIdentity() {
        return ownerIdentity;
    }

    public void setOwnerIdentity(OwnerIdentity ownerIdentity) {
        this.ownerIdentity = ownerIdentity;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

}
