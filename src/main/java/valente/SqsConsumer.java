package valente;

import com.amazonaws.services.sqs.model.Message;

/**
 * SqsConsumer
 */
public interface SqsConsumer {
    public void onNewMessage(Message message);
}