package valente;

import java.util.List;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityRequest;
import com.amazonaws.services.sqs.model.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SqsReader {

    private static final Logger logger = LoggerFactory.getLogger(SqsReader.class);

    // private AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

    private final SqsConsumer consumer;

    private final String queueName;

    public SqsReader(String queueName, SqsConsumer consumer) {
        this.queueName = queueName;
        this.consumer = consumer;
    }

    /**
     * Processa as mensagens e retorna o total de mensagens processadas
     */
    public int processMessages() {
        
        final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        final String queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
        final List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();
        logger.trace("recebidas " + messages.size() + " mensagens da fila.");
        for (Message message : messages) {
            final String handle = message.getReceiptHandle();
            logger.debug("processando mensagem " + handle);
            KeepAlive keepAlive = new KeepAlive(queueUrl, handle, 2);
            keepAlive.start();
            consumer.onNewMessage(message);
            keepAlive.stop();
            keepAlive.join();
            
            // sqs.changeMessageVisibility(new ChangeMessageVisibilityRequest(queueUrl, handle, 0));

            logger.trace("apagando mensagem apos processamento " + handle);
            sqs.deleteMessage(queueUrl, handle);
            logger.debug("mensagem processada e apagada " + handle);

        }
        return messages.size();
    }

    private static class KeepAlive {
        private boolean running = true;
        private Thread thread;
        // private final String queueUrl;
        // private final String handle;

        KeepAlive(String queueUrl, String handle, int interval) {
            // this.handle = handle;
            // this.queueUrl = queueUrl;

            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.debug("inicializando keep alive " + handle);
                    final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
                    while (running) {
                        logger.trace("enviando keep alive " + handle);
                        sqs.changeMessageVisibility(new ChangeMessageVisibilityRequest(queueUrl, handle, interval * 2));
                        try {
                            logger.trace("dormindo keep alive " + handle);
                            Thread.sleep(interval * 1000);
                        } catch (InterruptedException e) {
                            logger.trace("interrompendo keep alive " + handle);
                        }
                    }
                    logger.trace("terminando keep alive " + handle);
                }
            });
            
        }

        public void start () {
            thread.start();
        }

        public void stop () {
            running = false;
            try {
                thread.interrupt();
            } catch (Exception e) {

            }
        }

        public void join () {
            try {
                thread.join();
            } catch (InterruptedException e) {

            }
        }
    }
}