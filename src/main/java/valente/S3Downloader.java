package valente;

import java.io.File;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * S3Downloader
 */
public class S3Downloader {

    private static final Logger logger = LoggerFactory.getLogger(S3Downloader.class);

    private final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

    public void download(String bucketName, String key, File file) {
        logger.info("Download started:  " + bucketName + "://" + key + " into " + file.getAbsolutePath());
        try {
            logger.info("Download complete: " + bucketName + "://" + key + " into " + file.getAbsolutePath());
            s3.getObject(new GetObjectRequest(bucketName, key), file);
        } catch (Exception e) {
            logger.error("Erro fazendo o download de: " + bucketName + "://" + key + " into " + file.getAbsolutePath(), e);
        }
    }
}