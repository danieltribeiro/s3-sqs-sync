package valente;

import java.io.File;

import com.amazonaws.services.sqs.model.Message;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import valente.sqs.Record;

/**
 * SqsConsumerForS3Download
 */
public class SqsConsumerForS3Download implements SqsConsumer {

    @Override
    public void onNewMessage(Message message) {
        String json = message.getBody();

        JsonParser parser = new JsonParser();
        JsonObject object = parser.parse(json).getAsJsonObject();
        JsonObject messageObject = parser.parse(object.get("Message").getAsString()).getAsJsonObject();
        JsonArray records = messageObject.get("Records").getAsJsonArray();

        for (JsonElement e : records) {
            Record record = new Gson().fromJson(e, Record.class);
            String bucketName = record.getS3().getBucket().getName();
            String key = record.getS3().getObject().getKey().replaceAll("\\+", " ");
            String fileName = key;
            
            File file = new File("c:/temp/" + fileName);
            File parent = file.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }
            new S3Downloader().download(bucketName, key, file);
        }
    }
}